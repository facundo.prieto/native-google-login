# Prueba de concepto (React Native CLI)
Login con Google y grabar datos de la cuenta en una base (firebase como ejemplo) desde react-native con React Native CLI

[Lista de reproducción con los tutoriales paso a paso](https://www.youtube.com/playlist?list=PLsprmdocuVe9SvIQveOWqMvxHdkXjcRO_)  

## Crear un proyecto de React Native e instalar Firebase  

[Documentación](https://rnfirebase.io/)  

Desde una terminal ubicada en la carpeta donde queremos crear el proyecto ejecutar:  
```
npx react-native init nativegooglelogin --verbose
cd nativegooglelogin
npm install --save @react-native-firebase/app
```

### Generar credenciales Android:  

Crear un proyecto desde [Firebase](https://console.firebase.google.com/u/0/) y agregarle una app android  
- El "Android package name" está en la propiedad package del archivo `/android/app/src/main/AndroidManifest.xml`. En este caso es "com.nativegooglelogin"  

- Nickname: "native-google-login"
	
- Certificado de signing de debug (no es necesario ahora pero será necesario para agregar autenticación con Google)  
	- Desde una terminal ubicada en la carpeta del proyecto ejecutar:  
	- `cd android`  
	- `gradlew signingReport` o `./gradlew signingReport`
	- Copiar el 'SHA1' de la clave debugAndroidTest

- Descargar el archivo google-services.json y ponerlo en el proyecto de react-native en la carpeta `/android/app/`

### Configurar Firebase con credenciales de Android  

Agregar el plugin google-services como una dependencia en el archivo `/android/build.gradle`:  
```
buildscript {  
	dependencies {  
	// ...  
	classpath 'com.google.gms:google-services:4.3.10'  
	}  
}  
```
		
Ejecutar el plugin agregando lo siguiente al final de archivo `/android/app/build.gradle`:  
```
apply plugin: 'com.google.gms.google-services'  
```
			
Correr con `npx react-native run-android`  


## Agregar navecación para pruebas:  

Desde una terminal ubicada en la carpeta del proyecto ejecutar:  
```
npm install react-navigation  
npm install react-native-elements  
npm install react-navigation-stack  
```

Crear la carpeta ./screens y dentro poner una pantalla de carga y una de logueo  
	
Cambiar el App.js por un navegador entre esas pantallas usando createAppContainer y createSwitchNavigator de react-navigation

Correr con `npx react-native run-android`  

## Agregar autenticación con Firebase

[Documentación](https://rnfirebase.io/auth/usage)  

Desde una terminal ubicada en la carpeta del proyecto ejecutar `npm install @react-native-firebase/auth`  

En la pantalla de logueo, antes de la clase LoginScreen, agregar la función de la documentación que crea el listener onAuthStateChanged para saber si hay un usuario logueado o no.  

### Login anónimo  
Si se puede usar la app sin loguearse, pero se queire registrar quién la está usando, en la consola de Firebase habilitar Proveedor de acceso Anónimo para el proyecto.  
En el componentDidMount() de la clase LoginScreen agregar el lamado a la función de logueo anónimo auth().signInAnonymously().  
En la respueta de esa función se obtiene isNewUser, y un user con todo nulo excepto el user.uid.  
Esta lógica queda comentada dado que no es el obejtivo de esta prueba de concepto.

### Deslogueo  
Para cerrar sesión solo basta con agrar, dentro de la clase LoginScreen, una función que realice auth().signOut() y crear un botón que llame a esa función  

### Token de usuario  
Para obtener el token de la sesión que se enviará al backend, el SDK de Firebase ofrece las funciones getIdToken y getIdTokenResult.  
Una vez que se haya iniciado sesión, se puede obtener el token con `const idToken = await auth().currentUser.getIdToken();`  
O se puede obtener el token JWT con `const idTokenResult = await firebase.auth().currentUser.getIdTokenResult();`  
Firebase Authentication almacena en caché el token en el almacenamiento local y lo actualiza automáticamente cada hora en segundo plano (no se recomienda usar el user.uid como token).  

### Login con Google  

[Documentación](https://rnfirebase.io/auth/social-auth#google)  

Desde una terminal ubicada en la carpeta del proyecto ejecutar `npm install @react-native-google-signin/google-signin`  

Desde la consola de Firebase habilitar como Proveedor de acceso a Google para el proyecto.  

Al comienzo de la pantalla de login aregar:  
```
import { GoogleSignin } from '@react-native-google-signin/google-signin';

GoogleSignin.configure({
	webClientId: '',
});
```
El webClientId se puede encontrar en el archivo `android/app/google-services.json` como la propiedad `client/oauth_client/client_id`. Asegúrarse de elegir el client_id con `client_type: 3`

Dentro de la clase LoginScreen se agrega la función onGoogleButtonPress() de la documentación, y se agrega un botón que llame a esa función (tal como está en la documentación, pero agregando "this." en el llamado para que el onPress la encuentre).

Correr con `npx react-native run-android`  


### Login con Teléfono  

[Documentación](https://rnfirebase.io/auth/phone-auth)  

_Esto se sale del alcance de esta prueba de concepto, pero si el grupo lo cree pertinente se agregará en el futuro_

***
		
## Publicar en Git
Crear un repositorio native-google-login
Desde una terminal, ubicada en la carpeta del proyecto, ejecutar:
```
git remote add origin https://gitlab.com/facundo.prieto/native-google-login.git
git branch -M main
git add .
git commit -m "initial commit"
git push -uf origin main
```
