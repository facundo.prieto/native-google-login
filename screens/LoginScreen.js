import React, {Component, useState, useEffect} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
GoogleSignin.configure({
    webClientId: '836910618277-kfumtm1b0t9hrnv3nf2oq7d1qa1s01c5.apps.googleusercontent.com',
});

function LoginApp() {
    // Set an initializing state whilst Firebase connects
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();

    // Handle user state changes
    function onAuthStateChanged(user) {
        setUser(user);
        if (initializing) setInitializing(false);
    }

    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber; // unsubscribe on unmount
    }, []);

    if (initializing) return null;

    if (!user) {
        return (
        <View>
            <Text>Login</Text>
        </View>
        );
    }

    return (
        <View>
            <Text>Welcome {user.email}</Text>
        </View>
    );
}

export class LoginScreen extends Component {
    logoff = () => {
        auth()
        .signOut()
        .then(() => console.log('User signed out!'));
    }

    onGoogleButtonPress = async () => {
        // Get the users ID token
        const { idToken } = await GoogleSignin.signIn();
      
        // Create a Google credential with the token
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
      
        // Sign-in the user with the credential
        return auth().signInWithCredential(googleCredential);
    }

    componentDidMount(){
        // Dejo comentado el inicio de sesión anónimo 
        // auth()
        //     .signInAnonymously()
        //     .then((resp) => {
        //         console.log('User signed in anonymously');
        //         console.log(resp); //en la respueta tengo isNewUser y un user con todo nulo excepto el user.uid
        //     })
        //     .catch(error => {
        //         if (error.code === 'auth/operation-not-allowed') {
        //             console.log('Enable anonymous in your firebase console.');
        //         }

        //         console.error(error);
        //     });
    }

    LoadingScreen = ()=>{
        this.props.navigation.navigate('LoadingScreen');
    }
    
  render() {
    return (
        <View style={styles.container}>
            <LoginApp/>
            <Button
                title="Iniciar sesión con Google"
                onPress={() => this.onGoogleButtonPress().then(() => console.log('Signed in with Google!'))}
            />
            <Text> </Text>
            <Button title="Cerrar sesión" onPress={() => this.logoff()} />

            <Text> </Text>
            <Text>-----------</Text>
            <Text>LoginScreen</Text>
            <Button title="Volver a LoadingScreen" onPress={() => this.LoadingScreen()} />
        </View>
    );
  }
}

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
