import React, {Component} from 'react';
import { StyleSheet, Text, View, ActivityIndicator, Button} from 'react-native';

export class LoadingScreen extends Component {

    // Esta pantalla chequearía si el usuario está logueado o no, y lo redireccionaría al Home o la LoginScreen

    // componentDidMount() {
    //     this.checkIfLoggedIn();
    // }
    
    // checkIfLoggedIn = () => {
    //     var t = this;
    //     auth().onAuthStateChanged(function(user){
    //         console.log('Chequeo si hay un usuario logueado: ', user);
    //         if(user){
    //             //si existe sigue el flujo de la app, y se muestra un boton para desloguearse
    //             t.props.navigation.navigate('Home');
    //         }else{
    //             //si no hay un usuario logueado se redirecciona al login
    //             t.props.navigation.navigate('LoginScreen');
    //         }
    //     })
    // }

    LoginScreen = ()=>{
        this.props.navigation.navigate('LoginScreen');
    }

    render() {
        return ( 
            <View style={styles.container}>
                <ActivityIndicator size="large" color="#0000ff" />
                <Text> </Text>
                <Button title="LoginScreen" onPress={() => this.LoginScreen()} />
            </View>
        );
    }
}

export default LoadingScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
  