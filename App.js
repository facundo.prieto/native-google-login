import React, {Component} from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import LoadingScreen from './screens/LoadingScreen';
import LoginScreen from './screens/LoginScreen';

const AppSwitchNavigator = createSwitchNavigator({
  LoadingScreen: LoadingScreen,
  LoginScreen: LoginScreen
});

const AppNavigator = createAppContainer(AppSwitchNavigator);

export class App extends Component {
  render() {
    return ( 
      <AppNavigator/> 
      );
  }
}

export default App;